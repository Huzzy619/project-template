from rest_framework import serializers
from .models import Sample


class FirstSerializer(serializers.Serializer):
    name = serializers.CharField()
    age = serializers.IntegerField()

    def save(self, **kwargs):
        # Could also handle errors from here
        if False:
            raise serializers.ValidationError(
                detail="Invalid input data.......................",
            )
        return super().save(**kwargs)


# ======== OR ==========


class SecondSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = ["id", "name"]
