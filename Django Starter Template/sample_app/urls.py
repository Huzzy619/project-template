from . import views
from django.urls import path

urlpatterns = [
    path("sample", views.SampleView.as_view()),
]
