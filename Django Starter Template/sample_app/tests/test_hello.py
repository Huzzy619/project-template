# (Arrange, Act, Assert) - AAA

import random

import pytest
from faker import Faker
from model_bakery import baker
from rest_framework import status
from sample_app.models import Sample

FAKE = Faker()


@pytest.mark.django_db
class TestSampleCreation:
    def test_creating_sample_data_returns_200(self, api_client):
        # Arrange
        name = FAKE.name()
        data = {"name": name, "age": random.randint(10, 20)}

        # Act
        response = api_client.post(
            "/api/v1/sample",
            data,
            format="json",
        )

        # Assert
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["name"] == name

    def test_getting_sample_data_returns_200(self, api_client):
        # Arrange
        sample = baker.make(Sample)

        # Act
        response = api_client.get(
            "/api/v1/sample",
        )

        # Assert
        assert sample.name
        assert response.status_code == status.HTTP_200_OK
