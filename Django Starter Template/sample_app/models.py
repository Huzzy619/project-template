from core.models import BaseModel
from django.db import models


class Sample(BaseModel):
    name = models.CharField(max_length=100)
