from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from core.exception_handlers import ErrorEnum, ErrorResponse

from .serializers import FirstSerializer


class SampleView(GenericAPIView):
    serializer_class = FirstSerializer

    def get(self, request, *args, **kwargs):
        data = {"message": "Hello"}
        return Response(data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = FirstSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Any other business logic
        # serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        serializer = FirstSerializer(data=request.data)
        if serializer.is_valid():
            # Any other business logic
            # serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        # Another way to return Error response manually
        return ErrorResponse(ErrorEnum.ERR_001, serializer_errors=serializer.errors)
