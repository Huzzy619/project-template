from django.contrib import admin


class BlogAdminSite(admin.AdminSite):
    site_header = "ASMEA Administration"
    site_title = "ASMEA 023"
    index_title = "ASMEA Administration"
    empty_value_display = "- - - -"


admin_site = BlogAdminSite(name="ASMEA Admin Site")
