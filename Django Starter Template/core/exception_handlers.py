from enum import Enum

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import exception_handler


class ErrorEnum(Enum):
    ERR_001 = (
        "Validation Error",
        status.HTTP_400_BAD_REQUEST,
        "The request data failed validation. Please check your input and try again.",
    )
    ERR_002 = (
        "Authentication Error",
        status.HTTP_401_UNAUTHORIZED,
        "Authentication credentials were not provided.",
    )
    ERR_003 = (
        "Internal Server Error",
        status.HTTP_500_INTERNAL_SERVER_ERROR,
        "Something went wrong on our end. Please try again later.",
    )
    ERR_004 = (
        "Permission Error",
        status.HTTP_403_FORBIDDEN,
        (
            "Authentication credentials are either missing or the user lacks the"
            " necessary permissions to perform this action."
        ),
    )
    ERR_005 = (
        "Invalid Request Method",
        status.HTTP_405_METHOD_NOT_ALLOWED,
        "Accessing the resource with an unsupported method.",
    )


class ErrorResponse(Response):
    """
    Represents an error response to be returned from an API endpoint.

    Args:
        code (ErrorEnum): An enumeration representing the error code.
        serializer_errors (dict, optional): A dictionary of serializer validation errors.
            If provided, it would contain field names as keys and error information as values.
            Defaults to None.
        extra_detail (str, optional): Additional details or context for the error. Defaults to None.
        status (int, optional): The HTTP status code for the error response.
            If not provided, it defaults to the status associated with the provided error code.
        headers (dict, optional): Additional headers to include in the response. Defaults to None.

    Attributes:
        data (dict): A dictionary containing error information, including the error code,
            error message, and validation error details.
            If `serializer_errors` is provided, it includes validation error details for each field.
        status_code (int): The HTTP status code for the error response.

    Example:
        To create an error response with a specific error code and additional details:

        >>> error_response = ErrorResponse(
        ...     code=ErrorEnum.ERR_001,
        ...     extra_detail="Invalid input data",
        ...     headers={"X-Custom-Header": "Value"}
        ... )
    """

    def __init__(
        self,
        code: ErrorEnum,
        serializer_errors: dict = None,
        extra_detail: str = None,
        status: int = None,
        headers: dict = None,
    ):
        super().__init__(None, status=status or code.value[1])

        error_data = {
            "error_code": code.name,
            "error": code.value[0],
            "detail": (
                [
                    {"loc": ["body", field], "msg": error[0], "type": error[0].code}
                    for field, error in serializer_errors.items()
                ]
                if serializer_errors
                else code.value[2]
            ),
        }

        if extra_detail:
            error_data["extra_detail"] = extra_detail

        self.data = error_data

        if headers:
            for name, value in headers.items():
                self[name] = value


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(response.data, list):
        # The case of serializer.ValidationError been raised.
        response.data = {"non-field": response.data}

    if password_errors := check_password(response.data):
        # Remove any other password errors aside from the one processed and returned
        response.data = {
            k: v for k, v in response.data.items() if "password" not in v[0].code
        }
        response.data = response.data | password_errors

    if response:
        result = [code for code in ErrorEnum if code.value[1] == response.status_code]

        custom_response = ErrorResponse(
            code=result[0],
            status=response.status_code,
            serializer_errors=(
                response.data
                if response.status_code == status.HTTP_400_BAD_REQUEST
                else None
            ),
        )
        return custom_response
    response


def check_password(exc: dict):
    if error_list := exc.get("non_field_errors", None):
        if any("password" in error.code for error in error_list):
            error_dict = {
                f"password{index}": [error] for index, error in enumerate(error_list, 1)
            }

            return error_dict

    return None
